from sklearn.linear_model import SGDClassifier
from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt

# Datenimport
iris = datasets.load_iris()
pass
X = iris.data[:,:2]
Y = iris.target

# Durcheinanderwürfeln
indexmenge = np.arange(X.shape[0])
np.random.shuffle(indexmenge)

X = X[indexmenge]
Y = Y[indexmenge]

# Standardisierung
mean = X.mean(axis=0)
stabw = X.std(axis=0)
X = (X - mean) / stabw

clf = SGDClassifier(alpha=0.001, max_iter=100)
clf.fit(X, Y)

# Feldkoordinaten aufbauen
x_min, x_max = X[:,0].min()-1, X[:,0].max()+1
y_min, y_max = X[:,1].min()-1, X[:,1].max()+1
xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.02), np.arange(y_min, y_max, 0.02))

Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
Z = Z.reshape(xx.shape)
cs = plt.contourf(xx, yy, Z, cmap=plt.cm.Paired)

# Plot also the training points
for i, color in zip(clf.classes_, "bry"):
    idx = np.where(Y == i)
    plt.scatter(
        X[idx, 0],
        X[idx, 1],
        c=color,
        label=iris.target_names[i],
        cmap=plt.cm.Paired,
        edgecolor="black",
        s=20,
    )

# Plot the three one-against-all classifiers
xmin, xmax = plt.xlim()
ymin, ymax = plt.ylim()
coef = clf.coef_
intercept = clf.intercept_


def plot_hyperplane(c, color):
    def line(x0):
        return (-(x0 * coef[c, 0]) - intercept[c]) / coef[c, 1]

    plt.plot([xmin, xmax], [line(xmin), line(xmax)], ls="--", color=color)


for i, color in zip(clf.classes_, "bry"):
    plot_hyperplane(i, color)
plt.legend()

plt.show()


pass