# Trainingsmenge
X = [(0,1), (1,-1), (2,0), (2,1)]
Y = [1, 1, -1, -1]

X = [(el[0],el[1],1) for el in X]

class Perzeptron:
    def __init__(self, eta=1.0):
        # Konstruktorklasse
        self.eta = eta # Zuweisung der Schrittweite

    def _dot(self, x, y):
        # Implementierung des Skalarproduktes
        summe = 0
        for i in range(len(x)):
            summe += x[i]*y[i]
        return summe

    def _sum(self, x, y, diff=False):
        # Implementierung der Vektorsumme
        if diff == True:
            return tuple([x[i]-y[i] for i in range(len(x))])
        else:
            return tuple([x[i]+y[i] for i in range(len(x))])

    def _mult(self, k, x):
        # Implementierung der skalaren Multiplikation von Vektoren
        return tuple([k*x[i] for i in range(len(x))])

    def fit(self, X, Y):
        # Implementierung der Trainingsfunktion
        w = (0,0,0)     # Initialer Gewichtsvektor
        while True:     # Durchlaufe das Trainings-Set kontinuierlich
            classified = True   # Stop-Kriterium
            for i in range(0,len(X),1):  # Durchlaufe die Instanzen
                x = X[i]
                if Y[i] == 1:   # Falls das Label 1 ist ...
                    if self._dot(w, x) <= 0:
                        w = self._sum(w, self._mult(self.eta, x)) # w = w + eta * x
                        classified = False
                else:           # Falls das Label -1 ist ...
                    if self._dot(w, x) > 0:
                        w = self._sum(w, self._mult(self.eta, x), diff=True) # w = w - eta * x
                        classified = False
            if classified:
                break
        self.w = w
        return self

    def predict(self, X):
        # Implementierung der Predict-Funktion
        pass

p = Perzeptron()
p.fit(X, Y)