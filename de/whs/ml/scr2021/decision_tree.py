import numpy as np
from sklearn import datasets
import math

X = [
    [1, 0, 1],[1, 0, 1],[0, 0, 1],[1, 0, 0],[1, 1, 1],[1, 1, 1],
    [1, 1, 1],[0, 1, 1],[1, 1, 0],[1, 1, 0],[0, 1, 0]
]
Y = [
    1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0
]
X = np.array(X)
Y = np.array(Y)


class DecisionTree:

    def __init__(self, depth=0, max_depth=3):
        self.question = None
        self.answer = None
        self.information_gain = []
        self.children = []
        self.depth = depth
        self.max_depth = max_depth

    def fit(self, X, Y):
        if Y.size == 0:
            pass
        elif self._I(Y) == 1:
            self.answer = (Y[0], 1.0)
        elif self.depth > self.max_depth:
            a = np.argmax(self._P(Y))
            self.answer = (a, self._P(Y)[a])
        else:
            self.information_gain = self._G(Y, X)  #[0.12, 0.23, 0.21]
            self.question = np.argmax(self.information_gain)
            X0 = X[(X[::,self.question] == 0),::]
            X1 = X[(X[::,self.question] == 1),::]
            Y0 = Y[(X[::,self.question] == 0)]
            Y1 = Y[(X[::,self.question] == 1)]
            self.children.append(DecisionTree(depth=self.depth+1,max_depth=self.max_depth).fit(X0,Y0))
            self.children.append(DecisionTree(depth=self.depth+1,max_depth=self.max_depth).fit(X1,Y1))
        return self


    def _predict_single(self, x):
        if self.answer:
            return self.answer
        else:
            return self.children[x[self.question]]._predict_single(x)

    def predict(self, X):
        return [self._predict_single(x) for x in X]


    def _P(self, D): # [0,0,0,1,1,1,1,1,1,1]
        P = []
        count = len(set(D))
        P = [np.sum((D==i).astype(int))/D.size for i in range(count)]
        return P

    def _I(self, D): # 1 + 0.3 * math.log2(0.3) + 0.7 * math.log2(0.7)
        I = 1
        P = self._P(D)
        for i in range(len(P)):
            if P[i] > 0.0:
                I += P[i] * math.log2(P[i])
        return I

    def _G(self, D, X): # F = _P(X[::,0]) --> [0.6, 0.4], Y0=Y[(X[::,0] == 0)], Y1=Y[(X[::,0] == 1)] : Informationsgewinn: F[0]*_I(Y0) + F[1]*_I(Y1) - _I(Y)
        G = []
        for f in X.transpose():
            P = self._P(f)
            g = - self._I(D)
            for i in range(len(P)):
                d = D[(f==i)]
                g += P[i] * self._I(d)
            G.append(g)
        return G


iris = datasets.load_iris()
X_raw = iris.data
Y = iris.target
mean = np.mean(iris.data, axis=0)
X = (X_raw >= mean).astype(int)

from sklearn.model_selection import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.33, shuffle=True)

dt = DecisionTree()
dt.fit(X_train, Y_train)
pass
Y_pred = [item[0] for item in dt.predict(X_test)]

from sklearn.metrics import confusion_matrix
confusion_matrix(Y_test, Y_pred)