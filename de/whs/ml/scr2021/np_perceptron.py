import numpy as np
# Trainingsmenge
X = [(0,1), (1,-1), (2,0), (2,1)]
Y = [1, 1, -1, -1]

X = np.array([(el[0],el[1],1) for el in X])

class Perzeptron:
    def __init__(self, eta=1.0):
        # Konstruktorklasse
        self.eta = eta # Zuweisung der Schrittweite

    def fit(self, X, Y):
        # Implementierung der Trainingsfunktion
        w = np.zeros(3)     # Initialer Gewichtsvektor
        while True:     # Durchlaufe das Trainings-Set kontinuierlich
            classified = True   # Stop-Kriterium
            for i in range(0,X.shape[0],1):  # Durchlaufe die Instanzen
                x = X[i]
                if Y[i] == 1:   # Falls das Label 1 ist ...
                    if np.dot(w, x) <= 0:
                        w = w + self.eta * x # w = w + eta * x
                        classified = False
                else:           # Falls das Label -1 ist ...
                    if np.dot(w, x) > 0:
                        w = w - self.eta * x # w = w - eta * x
                        classified = False
            if classified:
                break
        self.w = w
        return self

    def predict(self, X):
        Y = (np.dot(X,self.w) >= 0).astype(int) - (np.dot(X,self.w) < 0).astype(int)
        # Implementierung der Predict-Funktion
        #Y = []
        #for i in range(0,X.shape[0]):
        #    if np.dot(X[i],self.w) >= 0: Y.append(1)
        #    else: Y.append(-1)
        #return Y

p = Perzeptron()
p.fit(X, Y)
p.predict(X)