# Objektorientierung
import math

class GeometrischeFigur:
    def flaeche(self):
        pass
    def umfang(self):
        pass
    def getPi(self):
        return math.pi


class Rechteck(GeometrischeFigur):
    def __init__(self, x, y, b, h):
        self.x = x
        self.y = y
        self.b = b
        self.h = h

    def flaeche(self):
        return self.b * self.h

    def umfang(self):
        return 2 * (self.b + self.h)

class Kreis(GeometrischeFigur):
    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r

    def flaeche(self):
        return self.r**2 * math.pi

    def umfang(self):
        return 2 * self.r * math.pi

r1 = Rechteck(1, 1, 2, 4)

r2 = Rechteck(1, 2, 3, 2)

k1 = Kreis(3, 3, 2)

l = [r1, r2, k1]

summe_flaeche = 0
for item in l:
    summe_flaeche += item.flaeche()

print(summe_flaeche)